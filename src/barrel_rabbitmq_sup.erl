%%%-------------------------------------------------------------------
%% @doc barrel_rabbitmq top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(barrel_rabbitmq_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
  BarrelChangesListener = {barrel_rabbitmq, {barrel_rabbitmq, start_link, []},
    transient, 5000, worker, []},
  
  {ok, {{one_for_one, 3, 10}, [BarrelChangesListener]}}.

%%====================================================================
%% Internal functions
%%====================================================================