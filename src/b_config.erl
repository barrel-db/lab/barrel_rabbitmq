%% @author Tah Teche <tahteche@gmail.com>
%% @copyright 2017 Tah Teche
%% @doc Extract config from config file.

%% Copyright 2017, Tah Teche
%%
%% Licensed under the Apache License, Version 2.0 (the "License"); you may not
%% use this file except in compliance with the License. You may obtain a copy of
%% the License at
%%
%%   http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
%% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
%% License for the specific language governing permissions and limitations under
%% the License.

-module(b_config).
-author("Tah Teche <tahteche@gmail.com>").

-compile(export_all).

%% @doc Get config map.
%%
config() ->
  {ok, Config} = application:get_env(barrel_rabbitmq, config),
  case is_map(Config) of
    true ->
      Config;
    _ ->
      throw({error, "Problem with config file" })
  end.

%% @doc Get server url.
%%
server() ->
  #{server := Server} = config(),
  list_to_binary(Server).

%% @doc List of URLs of the databases to monitor.
%%
databases() ->
  #{databases := Databases} = config(),
  Databases__Bin = [list_to_binary(X) || X <- Databases],
  Server = server(),
  MakeDbUrl = fun (Database) -> <<Server/binary, Database/binary>> end,
  lists:map(MakeDbUrl, Databases__Bin).

%% @doc Name to be assigned to the exchange used for publishing in RabbitMQ.
%%
exchange() ->
  #{exchange := Exchange} = config(),
  list_to_binary(Exchange).

%% @doc Name to be assigned to Turtle publishers and RabbitMQ queue
%% that will be used for publishing create events
create_event_publisher() ->
  #{event_publishers := Publishers} = config(),
  proplists:get_value(create, Publishers).

%% @doc Name to be assigned to Turtle publishers and RabbitMQ queue
%% that will be used for publishing update events
update_event_publisher() ->
  #{event_publishers := Publishers} = config(),
  proplists:get_value(update, Publishers).

%% @doc Name to be assigned to Turtle publishers and RabbitMQ queue
%% that will be used for publishing delete events
delete_event_publisher() ->
  #{event_publishers := Publishers} = config(),
  proplists:get_value(delete, Publishers).

connection() ->
  {ok, TurtleConfig} = application:get_env(turtle, connection_config),
  [First | _] = TurtleConfig,
  #{conn_name := Connection} = First,
  Connection.