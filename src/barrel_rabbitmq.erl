%% @author Tah Teche <tahteche@gmail.com>
%% @copyright 2017 Tah Teche
%% @doc Publish Barrel events through RabbitMQ.

%% Copyright 2017, Tah Teche
%%
%% Licensed under the Apache License, Version 2.0 (the "License"); you may not
%% use this file except in compliance with the License. You may obtain a copy of
%% the License at
%%
%%   http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
%% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
%% License for the specific language governing permissions and limitations under
%% the License.

-module(barrel_rabbitmq).
-author("Tah Teche <tahteche@gmail.com>").

-behaviour(gen_server).

%% API
% -export([
%   start_publishing_changes/0,
%   stop_publishing_changes/0
%   ]).

% -export([start_link/0]).

% %% gen_server callbacks
% -export([init/1,
%          handle_call/3,
%          handle_cast/2,
%          handle_info/2,
%          terminate/2,
%          code_change/3]).

-record(state, {}).

-compile(export_all).

start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

stop() ->
  gen_server:stop(barrel_rabbitmq).

%% gen_server

init([]) ->
  process_flag(trap_exit, true),
  self() ! {start_publishers},
  {ok, #state{}}.

handle_call(_Request, _From, State) ->
  {reply, ignored, State}.

handle_cast(_Msg, State) ->
  {noreply, State}.

handle_info({start_publishers}, State) ->
  start_publishers(),
  start_publishing_changes(),
  {noreply, State}.

terminate(_Reason, _State) ->
  stop_publishing_changes(),
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%% Internal Functions

%% @doc Get events happening on a database and start pushing them to RabbitMQ
%%
%% This is the main interface to the whole barrel_rabbitmq application
%% @end
% -spec start_publishing_changes(DB)-> {ok, pid()} when
%   DB :: binary().
start_publishing_changes() ->

  start_publishers(),

  %% Get URL for only the first database in the server config.
  %% TODO: Monitor multiple databases.
  [ DB_URL | _ ] = b_config:databases(),

  {ok, _} = application:ensure_all_started(barrel_httpc),

  case barrel_httpc:connect(DB_URL) of
  {ok, Conn} ->
    Callback = fun changes_cb/1,

    ListenerOptions = #{since => 0, mode => sse, changes_cb => Callback },

    {ok, ListenerPid} = barrel_httpc:start_changes_listener(Conn, ListenerOptions),
    gproc:reg_other({n, l, watched_database}, ListenerPid, DB_URL),
    {ok, ListenerPid};

  _ -> throw({error, "Error connecting to Barrel server/database"}) 
  end.

%% @doc Stop publishing events to RabbitMQ.
%%
%% This stops publishing of events to RabbitMQ but does not kill the child publisher processes.
%% @end

%% @TODO This should stop the whole application including stoping child processes 
-spec stop_publishing_changes()-> ok.
stop_publishing_changes() ->
  ListenerPid = gproc:lookup_pid({n, l, watched_database}),
  barrel_httpc:stop_changes_listener(ListenerPid).

%%Callback function which processes changes
changes_cb(Change) ->
  Exchange = b_config:exchange(),
  CreateEventPublisher = b_config:create_event_publisher(),
  UpdateEventPublisher = b_config:update_event_publisher(),
  DeleteEventPublisher = b_config:delete_event_publisher(),
  case maps:is_key(<<"deleted">>, Change) of
  true ->
    Change1 = Change#{<<"status">> => <<"deleted">>},
    ChangeJSON = jsone:encode(Change1),
    io:format("~n~p~n", [ChangeJSON]),
    b_rabbitmq:publish_message(DeleteEventPublisher, Exchange, atom_to_binary(DeleteEventPublisher, latin1), ChangeJSON);
  _ ->
    #{<<"rev">> := Rev} = Change,
    [Sequence, _] = binary:split(Rev, <<"-">>),
    case Sequence =:= <<"1">> of
    true ->
      Change1 = Change#{<<"status">> => <<"created">>},
      ChangeJSON = jsone:encode(Change1),
      io:format("~n~p~n", [ChangeJSON]),
      b_rabbitmq:publish_message(CreateEventPublisher, Exchange, atom_to_binary(CreateEventPublisher, latin1), ChangeJSON);
    false ->
      Change1 = Change#{<<"status">> => <<"updated">>},
      ChangeJSON = jsone:encode(Change1),
      io:format("~n~p~n", [ChangeJSON]),
      b_rabbitmq:publish_message(UpdateEventPublisher, Exchange, atom_to_binary(UpdateEventPublisher, latin1), ChangeJSON)
    end
  end.

start_publishers() ->
  Exchange = b_config:exchange(),
  ConnName = b_config:connection(),
  
  CreateEventPublisher = b_config:create_event_publisher(),
  CreateEventPublisher_Bin = atom_to_binary(CreateEventPublisher, latin1),
  CreateEventPublisherSpec = b_rabbitmq:create_publisher_spec(
  CreateEventPublisher, ConnName, Exchange, CreateEventPublisher_Bin),
  supervisor:start_child(barrel_rabbitmq_sup, CreateEventPublisherSpec),

  UpdateEventPublisher = b_config:update_event_publisher(),
  UpdateEventPublisher_Bin = atom_to_binary(UpdateEventPublisher, latin1),
  UpdateEventPublisherSpec = b_rabbitmq:create_publisher_spec(
  UpdateEventPublisher, ConnName, Exchange, UpdateEventPublisher_Bin),
  supervisor:start_child(barrel_rabbitmq_sup, UpdateEventPublisherSpec),

  DeleteEventPublisher = b_config:delete_event_publisher(),
  DeleteEventPublisher_Bin = atom_to_binary(DeleteEventPublisher, latin1),
  DeleteEventPublisherSpec = b_rabbitmq:create_publisher_spec(
  DeleteEventPublisher, ConnName, Exchange, DeleteEventPublisher_Bin),
  supervisor:start_child(barrel_rabbitmq_sup, DeleteEventPublisherSpec).