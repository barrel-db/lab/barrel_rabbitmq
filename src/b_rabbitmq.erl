%% @author Tah Teche <tahteche@gmail.com>
%% @copyright 2017 Tah Teche
%% @doc Connect to RabbitMQ via Turtle.

%% Copyright 2017 Tah Teche
%%
%% Licensed under the Apache License, Version 2.0 (the "License"); you may not
%% use this file except in compliance with the License. You may obtain a copy of
%% the License at
%%
%%   http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
%% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
%% License for the specific language governing permissions and limitations under
%% the License.

-module(b_rabbitmq).
-author("Tah Teche <tahteche@gmail.com>").

%% API
-export([
  start_publisher/4,
  create_publisher_spec/4,
  publish_message/4
  ]).

-include_lib("amqp_client/include/amqp_client.hrl").

%% Type Definitions
-type child_spec() :: #{id => child_id(),       % mandatory
                       start => mfargs(),      % mandatory
                       restart => restart(),   % optional
                       shutdown => shutdown(), % optional
                       type => worker(),       % optional
                       modules => modules()}.   % optional
-type child_id() :: term().
-type mfargs() :: {M :: module(), F :: atom(), A :: [term()]}.
-type modules() :: [module()] | dynamic.
-type restart() :: permanent | transient | temporary.
-type shutdown() :: brutal_kill | timeout().
-type worker() :: worker | supervisor.

%% @doc Start a new publisher process under the Turtle supervisor.
%% @end
-spec start_publisher(PublisherName, ConnectionName, Exchange, Queue)-> {ok, pid()} when
  PublisherName :: atom(),
  ConnectionName :: atom(),
  Exchange :: binary(),
  Queue :: binary().
start_publisher(PublisherName, ConnectionName, Exchange, Queue)->
    AMQPDecls = [
      #'exchange.declare' { exchange = Exchange, type = <<"topic">>, durable = true },
      #'queue.declare' { queue = Queue, durable = true },
      #'queue.bind' { queue = Queue, exchange = Exchange, routing_key = Queue }
    ],
    {ok, _Pid} = turtle_publisher:start_link(PublisherName, ConnectionName, AMQPDecls,
                #{ confirms => false} ).

%% @doc Create a ChildSpec for a publisher process.
%% @end
-spec create_publisher_spec(PublisherName, ConnectionName, Exchange, Queue)-> child_spec() when
  PublisherName :: atom(),
  ConnectionName :: atom(),
  Exchange :: binary(),
  Queue :: binary().
create_publisher_spec(PublisherName, ConnectionName, Exchange, Queue) ->
  AMQPDecls = [
    #'exchange.declare' { exchange = Exchange, type = <<"topic">>, durable = true },
    #'queue.declare' { queue = Queue, durable = true },
    #'queue.bind' { queue = Queue, exchange = Exchange, routing_key = Queue }
  ],
  turtle_publisher:child_spec(PublisherName, ConnectionName, AMQPDecls,
  #{ confirms => true, passive => false }).

%% @doc Publish a message using a Turtle publisher.
%% @end
-spec publish_message(Publisher, Exchange, Queue, Payload)-> ok when
  Publisher :: atom(),
  Exchange :: binary(),
  Queue :: binary(),
  Payload :: iodata().
publish_message(Publisher, Exchange, Queue, Payload) ->  
  turtle:publish(Publisher,
    Exchange,
    Queue,
    <<"application/json">>,
    Payload,
    #{ delivery_mode => ephemeral }).