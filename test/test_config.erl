%% @author Tah Teche <tahteche@gmail.com>
%% @copyright 2017 Tah Teche
%% @doc Load application configs for barrl_rabbitmq and turtle during test.
%%
%% This is needed because sys.config can't be loaded during test. Consider this the sys.config for test. 

%% Copyright 2017, Tah Teche
%%
%% Licensed under the Apache License, Version 2.0 (the "License"); you may not
%% use this file except in compliance with the License. You may obtain a copy of
%% the License at
%%
%%   http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
%% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
%% License for the specific language governing permissions and limitations under
%% the License.

-module(test_config).
-author("Tah Teche").


%% API
-export([
  load_barrel_rabbitmq_config/0,
  load_turtle_config/0
]).

load_barrel_rabbitmq_config() ->
  BRabbitMQConfig = #{
    %% Barrel server and the list of databases to monitor on it.
    %% Possible to monitor only one database. Multi-databases monitoring will be added.
    server => "http://localhost:7080/dbs/",
    databases => ["testdb"],

    %% Name of the exchange to be used in RabbitMQ
    exchange => "barrel",

    %%Assign the name of publishers to be used for the 3 event types.
    %%Note that queues in RabbitMQ will be assigned the same names.
    event_publishers => [
      {create, create_event_publisher}, 
      {update, update_event_publisher}, 
      {delete, delete_event_publisher}
    ]
  },
application:set_env(barrel_rabbitmq, config, BRabbitMQConfig, [{persistent, true}]).

load_turtle_config() ->
  TurtleConfig = [
    #{
      conn_name => amqp_server,

      %% Username, password and virtual host of your rabbitmq server
      username => "guest",
      password => "guest",
      virtual_host => "/",

      %% Assign avalailable rabbitmq instances. Instances are placed in groups.
      %%
      %% The first group is the Danville group with two hosts and the second is the Doofenschmirtz group. 
      %% Turtle will try amqp-1 and amqp-2 in a round-robin fashion for a while.
      %% Upon failure it will fall back to the backup group.
      %%
      %% connections => [
      %%  {main, [
      %%   {"amqp-1.danville.com", 5672 },
      %%   {"amqp-2.danville.com", 5672 } ]},
      %%  {backup, [
      %%   {"backup.doofenschmirtz.com", 5672 } ]} ]

      connections => [
        {main, [
          {"localhost", 5672 }]}]
    }],
  application:set_env(turtle, connection_config, TurtleConfig, [{persistent, true}]).