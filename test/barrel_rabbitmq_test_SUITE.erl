%% Copyright 2017, Tah Teche
%%
%% Licensed under the Apache License, Version 2.0 (the "License"); you may not
%% use this file except in compliance with the License. You may obtain a copy of
%% the License at
%%
%%   http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
%% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
%% License for the specific language governing permissions and limitations under
%% the License.

-module(barrel_rabbitmq_test_SUITE).
-author("Tah Teche").


%% API
-export([
  all/0,
  init_per_suite/1,
  end_per_suite/1,
  init_per_testcase/2,
  end_per_testcase/2
]).

-export([
  create_event/1
]).

all() ->
  [
   create_event
  ].

init_per_suite(Config) ->
  test_config:load_barrel_rabbitmq_config(),
  test_config:load_turtle_config(),
  {ok, _} = application:ensure_all_started(barrel_rabbitmq),
  {ok, BarrelConfig} = application:get_env(barrel_rabbitmq, config),

  #{server := Server} = BarrelConfig,
  Server__Bin = list_to_binary(Server),

  #{databases := Databases} = BarrelConfig,
  [Database | _ ] = Databases,
  Database__Bin = list_to_binary(Database),

  DB_URL = <<Server__Bin/binary, Database__Bin/binary>>,

  [{db_name, Database__Bin}, {server, Server__Bin}, {db_url, DB_URL}] ++ Config.

init_per_testcase(_, Config) ->
  barrel_httpc:create_database(proplists:get_value(db_url, Config)),
  Config.

end_per_testcase(_, Config) ->
  _ = barrel_httpc:delete_database(proplists:get_value(db_url, Config)),
  ok.

end_per_suite(Config) ->
  _ = application:stop(barrel_rabbitmq),
  Config.

create_event(Config)->
  <<"testdb">> = proplists:get_value(db_name, Config).